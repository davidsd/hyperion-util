{-# LANGUAGE OverloadedStrings #-}

module Hyperion.Util.ToPath
  ( ToPath(..)
  , mkTmpFilePath
  , dirScatteredHashBasePath
  , hashBasePath
  ) where

import Data.Binary           (Binary)
import Data.BinaryHash       (hashUntypedBase64Safe)
import Hyperion.Util         (randomString)
import System.FilePath.Posix ((</>))

-- | A class for objects that can be assigned a FilePath, given a base
-- directory
class ToPath a where
  toPath
    :: FilePath -- ^ base directory
    -> a        -- ^ Object
    -> FilePath

hashBasePath :: Binary a => String -> a -> FilePath
hashBasePath tag a = tag <> "_" <> hashUntypedBase64Safe a

-- | Construct a path from a hash, with a directory made from the
-- first two characters of the hash to avoid putting too many files in
-- a single directory
dirScatteredHashBasePath :: Binary a => String -> a -> FilePath
dirScatteredHashBasePath tag obj = hashDir </> hashFile
  where
    (hashDir, hashFile) = case hashUntypedBase64Safe obj of
      s@(x : y : _) -> ("a"<>[x,y], tag <> "_" <> s)
      s             -> error $ "Expected a longer table hash: " <> s

mkTmpFilePath :: FilePath -> IO FilePath
mkTmpFilePath f = do
  let tmpstem = f <> ".tmp-"
  rand <- randomString 5
  return (tmpstem <> rand)
