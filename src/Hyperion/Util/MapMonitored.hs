{-# LANGUAGE OverloadedStrings #-}

module Hyperion.Util.MapMonitored
  ( mapChunksConcurrentlyMonitored
  , mapConcurrentlyMonitored
  , setShuffled
  , setShuffledChunks
  ) where

import Control.Concurrent      (forkIO, killThread, threadDelay)
import Control.Concurrent.MVar (modifyMVar_, newMVar, tryReadMVar)
import Control.Monad           (when)
import Control.Monad.IO.Class  (liftIO, MonadIO)
import Data.List               (sortOn)
import Data.List.Split         (chunksOf)
import Data.Set                (Set)
import Data.Set                qualified as Set
import Data.Text               (Text)
import Data.Time               (NominalDiffTime, diffUTCTime, getCurrentTime)
import Hyperion                (mapConcurrently_, Concurrently)
import Hyperion.Log            qualified as Log
import Hyperion.Util           (nominalDiffTimeToMicroseconds)
import System.Random           (mkStdGen, randoms)
import Text.Printf             qualified as Printf

mapChunksConcurrentlyMonitored
  :: (MonadIO m, Applicative (Concurrently m))
  => Text
  -> NominalDiffTime
  -> ([k] -> m ())
  -> [[k]]
  -> m ()
mapChunksConcurrentlyMonitored tag reportInterval handleChunk chunks = do
  let numKeys = sum (map length chunks)
  when (numKeys > 0) $ do
    Log.info ("Computing " <> tag) numKeys
    startTime <- liftIO getCurrentTime
    todoVar <- liftIO $ newMVar numKeys
    let
      go chunk = do
        handleChunk chunk
        liftIO $ modifyMVar_ todoVar $ pure . (subtract (length chunk))
      monitor lastTodo = do
        threadDelay $ nominalDiffTimeToMicroseconds reportInterval
        mNumTodo <- tryReadMVar todoVar
        case mNumTodo of
          Just n
            | n > 0 -> do
                when (n < lastTodo) $
                  Log.text $ tag <> " progress: " <> Log.showText (progressBar (numKeys - n) numKeys)
                monitor n
          _ -> pure ()
    monitorThreadId <- liftIO $ forkIO $ monitor $ numKeys + 1
    mapConcurrently_ go chunks
    liftIO $ killThread monitorThreadId
    endTime <- liftIO getCurrentTime
    Log.info ("Finished computing " <> tag) (numKeys, diffUTCTime endTime startTime)

mapConcurrentlyMonitored
  :: (MonadIO m, Applicative (Concurrently m))
  => Text
  -> NominalDiffTime
  -> (k -> m ())
  -> [k]
  -> m ()
mapConcurrentlyMonitored tag reportInterval go ks =
  mapChunksConcurrentlyMonitored tag reportInterval (mapM_ go) (map pure ks)

-- | Shuffle the list using a deterministic pseudo-random generator
shuffle :: [a] -> [a]
shuffle xs =
  map fst $ sortOn snd $ zip xs $ randoms @Int (mkStdGen 137)

setShuffled :: Set k -> [k]
setShuffled = shuffle . Set.toList

setShuffledChunks :: Int -> Set k -> [[k]]
setShuffledChunks chunkSize = chunksOf chunkSize . setShuffled

data ProgressBar = MkProgressBar Rational Rational

progressBar :: Real a => a -> a -> ProgressBar
progressBar x y = MkProgressBar (toRational x) (toRational y)

instance Show ProgressBar where
  show (MkProgressBar n total) =
    "[" ++ addArrow (replicate nEquals '=') ++ replicate nSpaces ' ' ++ "]" ++
    Printf.printf " %d/%d (%.1f%%)" (ceiling @_ @Int n) (ceiling @_ @Int total) percent
    where
      percent = 100 * fromRational @Double (n/total)
      nEquals = ceiling $ toRational len * n/total
      nSpaces = len - nEquals
      len = 40
      addArrow []     = []
      addArrow (_:cs) = reverse $ '>':cs
